const axios = require('axios');

const getWeather = async ( lat , lng) => {
  const infoWeather = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=b922369a2ae15e3cfcd6d03e2c7cf568&units=metric`)
  return infoWeather.data.main.temp
}
module.exports = {
    getWeather
}
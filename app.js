const place = require('./place/place');
const weather = require('./weather/weather')

const argv = require('yargs').options({
    address: {
        alias: 'a',
        desc: 'Address the city for get the weather',
        demand: true
    }
}).argv

const logic = async (address) => {
    try {
        let info = await place.getPlaceLattAndLongt(address)
        let weatherInfo = await weather.getWeather(info.lat , info.lng) 
        return `The Weather in ${info.address} is ${weatherInfo}`
    } catch (error) {
        return `Error`
    }
    
}
logic(argv.address).then(res => console.log(res))

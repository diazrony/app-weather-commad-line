const axios = require('axios');

const getPlaceLattAndLongt = async (address) => {
    const encodedUrl = encodeURI(address)
    const resp = await axios.get(`https://geocode.xyz/${encodedUrl}?json=1`)

   
    const data = resp.data
    const country = data.standard.countryname
    const lng = data.longt
    const lat = data.latt
    return {
        address,
        country,
        lat,
        lng
    }
        
}

module.exports = {
    getPlaceLattAndLongt
}